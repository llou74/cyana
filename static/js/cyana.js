(function () {
    var body = document.body, html = document.documentElement;
    var accionador = document.getElementById("hamburguesa");
    var visible = false;
    var ancho_hamburguesa = 40;


    if (document.documentElement.clientWidth > 767) {
        var panel = document.getElementById("menu");
        var cuerpo= document.getElementById("cuerpo");
        var ancho_menu = 240;
        var escondido_menu = ancho_hamburguesa-ancho_menu;

        function actualiza() {
                panel.style.left = visible ? 0 : escondido_menu+"px";
                cuerpo.style.left = visible ? ancho_menu+"px" : ancho_hamburguesa+"px" ;
        }

        accionador.addEventListener('click', function() {
            visible = !visible;
            actualiza();
        });


        var web_height = Math.max( body.scrollHeight, body.offsetHeight, 
               html.clientHeight, html.scrollHeight, html.offsetHeight ); 
        panel.style.height = web_height;

    } else {

        var contenido = document.getElementById("contenido-menu");
        var alturaContenido = 100;

        function actualiza() {
                contenido.style.top = visible ? 0 : "-"+alturaContenido+"px";
        }

        accionador.addEventListener('click', function() {
            visible = !visible;
            actualiza();
        });
    }

    function obten_traduccion() {
        function getMeta(metaName) {
          const metas = document.getElementsByTagName('meta');
          for (let i = 0; i < metas.length; i++) {
            if (metas[i].getAttribute('name') === metaName) {
              return metas[i].getAttribute('content');
            }
          }
          return '';
        }
        return getMeta('traduccion');
    }

    var lang = document.documentElement.lang;
    var bandera = document.getElementById("bandera");
    var traduccion = obten_traduccion()

    bandera.addEventListener('click', function() {
        window.open(traduccion, '_self');
    });

    if (lang == "EN") {
        bandera.style.backgroundImage = "url(static/css/es.svg)";
    } else {
        bandera.style.backgroundImage = "url(static/css/gb.svg)";
    }

})();
